#!/bin/bash

# connects to cluster in gcloud
gcloud container clusters get-credentials $(gcloud container clusters list | sed -n '2 p' | awk '{print $1}') --region us-central1 --project $(gcloud config list --format 'value(core.project)')

# clones official helm chart repo to user home directory
git clone https://github.com/helm/charts/ ~/charts 

