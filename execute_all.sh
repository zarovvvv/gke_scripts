#!/bin/bash


# Copy tf var to tf repo


STARTTIME=$(date +%s)

# Execute all scripts

echo "Start execution"
echo "execute 0_clone_tf_repo_cp_vars.sh" && ./0_clone_tf_repo_cp_vars.sh &&  \
echo "execute 1_enable_apis.sh" && ./1_enable_apis.sh && \
echo "execute 2_tf_create_svcacc.sh" && ./2_tf_create_svcacc.sh && \
echo "execute 3_execute_tf.sh" && ./3_execute_tf.sh && \
echo "execute 4_connect_to_cluster_clone_repos.sh" && ./4_connect_to_cluster_clone_repos.sh && \
echo "execute 5_install_nfs_storage_provisioner.sh" && ./5_install_nfs_storage_provisioner.sh && \
echo "All scripts executed successfully"

# Shows ecution time

ENDTIME=$(date +%s)
EXECUTION_TIME=$(($ENDTIME - $STARTTIME))

if [ "$EXECUTION_TIME" -gt '60' ]; then
echo "All scripts executed in $((EXECUTION_TIME / 60)) minutes."
else
echo "All scripts executed in $EXECUTION_TIME seconds."
fi
