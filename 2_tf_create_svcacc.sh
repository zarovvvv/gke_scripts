#!/bin/bash

# create service account with name terraform-svc
gcloud iam service-accounts create terraform-svc \
--description "with owner permission for terraform" \
--display-name "terraform-svc"


# bind service account container.clusterAdmin permissions
gcloud projects add-iam-policy-binding $(gcloud config list --format 'value(core.project)') \
--member serviceAccount:terraform-svc@$(gcloud config list --format 'value(core.project)').iam.gserviceaccount.com \
--role roles/container.clusterAdmin

# bind service account iam.serviceAccountUser permissions
gcloud projects add-iam-policy-binding $(gcloud config list --format 'value(core.project)') \
--member serviceAccount:terraform-svc@$(gcloud config list --format 'value(core.project)').iam.gserviceaccount.com \
--role roles/iam.serviceAccountUser


# bind service account permissions
gcloud projects add-iam-policy-binding $(gcloud config list --format 'value(core.project)') \
--member serviceAccount:terraform-svc@$(gcloud config list --format 'value(core.project)').iam.gserviceaccount.com \
--role roles/compute.instanceAdmin


# create key for service account and store it to account.json 
gcloud iam service-accounts keys create account.json \
--iam-account terraform-svc@$(gcloud config list --format 'value(core.project)').iam.gserviceaccount.com

# move serviceaccount key to terraform folder
mv account.json gke_terraform/
