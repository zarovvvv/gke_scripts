variable "region" {
  description = "Zone name here will create Zonal GKE, Regione name - Regional"
  type        = string
#  default     = "us-central1-c" # Example for Zonal Cluster
#  default     = "us-central1"   # Example for Regional Cluster
}

variable "node_count" {
  description = "WARNING: if Cluster is Regional, specified number will create node in every zone!"
  type        = number
#  default     = 3 # IF var.gerion is set to Region, that will create 3 node in every Zone - 9 Nodes Total 
}

variable "workspace" {
  description = "Name of GKE cluster."
  type        = string
#  default     = "dev-us-central" # Example for Workspace name
}

variable "master_version" {
  description = "Kubernetes Masters Version"
  type        = string
#  default     = "1.17.9-gke.6300" # Example for how to set Master version
}

variable "node_version" {
  description = "Kubernetes Nodes Version"
  type        = string
#  default     = "1.17.9-gke.6300" # Example of how to set Node version
}

variable "machine_type" {
  description = "Kubernetes Nodes VM Type"
  type        = string
#  default     = "n1-standard-1" # Example for how to set VM Type to n1-standard-1

}

variable "preemptible" {
  description = "If true, Node VM's will be Type Preemptible"
  type        = string
#  default     = "false" # Example for how to create normal type VM's
}

variable "project" {
  description = "GCP Project ID that Terraform will use"
#  default     = "uscentral-4542188748132895" # Example for how to set Project ID
}
