#!/bin/bash

# install nfs-storage-provisioner in nfs namespace 
kubectl create namespace nfs
helm install nfs --namespace nfs -f ~/charts/stable/nfs-server-provisioner/values.yaml ~/charts/stable/nfs-server-provisioner

