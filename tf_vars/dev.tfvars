variable "region" {
  description = "Zone name here will create Zonal GKE, Regione name - Regional"
  type        = string
  default     = "us-central1"
}

variable "node_count" {
  description = "WARNING: if Cluster is Regional, specified number will create node in every zone!"
  type        = number
  default     = 3
}

variable "workspace" {
  description = "Name of GKE cluster."
  type        = string
  default     = "gke-dev-us-central" 
}

variable "master_version" {
  description = "Kubernetes Masters Version"
  type        = string
  default     = "1.17.9-gke.6300" 
}

variable "node_version" {
  description = "Kubernetes Nodes Version"
  type        = string
  default     = "1.17.9-gke.6300" 
}

variable "machine_type" {
  description = "Kubernetes Nodes VM Type"
  type        = string
  default     = "n1-standard-1" 

}

variable "preemptible" {
  description = "If true, Node VM's will be Type Preemptible"
  type        = string
  default     = "false" 
}

variable "project" {
  description = "GCP Project ID that Terraform will use"
}
