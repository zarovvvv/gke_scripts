#!/bin/bash

# clone Terraform
git clone https://gitlab.com/zarovvvv/gke_terraform.git 

# Copy  vars to terraform folder

display_usage() { 
        echo "Choose one of [sandbox, dev, custom]" 
        }   

printf "Select type of Environment [sandbox, dev or custom]: "
read case
case $case in

# Copying premade var files based on choice.
sandbox)  echo "Sandbox Environment"
         cp tf_vars/sandbox.tfvars gke_terraform/vars.tf 
    ;;  
dev)     echo  "Dev Environment"
         cp tf_vars/dev.tfvars gke_terraform/vars.tf 
    ;;  
# You can add default values or leave them empty, in this case Terraform will ask for every var
custom)  echo  "Custom Environment"
         cp tf_vars/custom.tfvars gke_terraform/vars.tf
    ;;       
*)    display_usage
   ;;
esac  
