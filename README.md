# For GKE Environments Only!
 
### TL/DR
Login in GKE Console, open Cloud Shell and run:
```bash
git clone https://gitlab.com/zarovvvv/gke_scripts.git
cd gke_scripts && ./execute_all.sh
```

### What does this scripts do:

- It provisions managed Kubernetes cluster **(GKE)** in Google Cloud with **Terraform**  
- Connects to cluster and installs helm 3 and NFS storage provisioner for GKE inside separated namespaces 
- You can use nfs as storage class for Kubernetes  

## Set Terraform Vars
Script 0 clones TF repo and copies TF vars, based on your choice.
If you want to create own configuration, edit **tf_vars/custom.tfvars** and then execute execute_all.sh and chooce **"custom"**

## execute_all.sh
Will execute all other scripts in order from 0 to 5

- 0 # Clones TF repo and copies TF var, based on your choice
- 1 # Enables all GCP apis needed
- 2 # Creates service account for terraform
- 3 # Executes terraform apply
- 4 # Connects to cluster and clones official helm repo charts - https://github.com/helm/charts/tree/master/stable
- 5 # Installs nfs storage provisioner from helm Google helm chart - capable of auto provisioning **ReadWriteMany** volumes for Kubernetes!

